import MyComponent from '../../component.vue';

export default {
	title: 'VueStory',
	component: MyComponent
}
export function story() {
	return {
		name: 'VueComponentStory',
		components: {MyComponent},
		template: `
			<MyComponent content="JS Story"></MyComponent>
		`
	};
}
